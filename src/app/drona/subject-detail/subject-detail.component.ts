import { ExamService } from './../../backend/exam.service';
import { NgForm } from '@angular/forms';
import { ShopService } from './../../backend/shop.service';
import { NavigationExtras, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
declare var $: any;
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;

@Component({
  selector: 'app-subject-detail',
  templateUrl: './subject-detail.component.html',
  styleUrls: ['./subject-detail.component.scss']
})
export class SubjectDetailComponent implements OnInit {

  Subject: any = new Object()
  Attainment: any = new Object()

  constructor(private router: Router, private shopService: ShopService, private examService: ExamService) {
    try {
      console.log("Fetched from Nav Extras");
      this.Subject.subject_id = this.router.getCurrentNavigation().extras.state.subject_id
      console.log(this.Subject);
    } catch (error) {
      console.log("!! Fetched from Storage");
      this.getItem()
    }
  }


  ngOnInit(): void {
    this.list_coutcomes()
    this.list_attainments()
    $(".preloader").fadeOut();

  }


  async getItem() {
    const { value } = await Storage.get({ key: 'subject_id' });
    this.Subject.subject_id = value
    console.log('Got item: ', value);
  }

  isLoading = false
  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Factor List // Students [ student_ ]
  Attainments: any
  Exams: any
  list_attainments() {
    this.shopService.attainment_list({ subject: this.Subject.subject_id }).subscribe(
      resp => {
        this.Attainments = resp
        console.log(resp);
      }
    )
    this.examService.exam_list({ subject_id: this.Subject.subject_id }).subscribe(
      resp => {
        this.Exams = resp
        console.log(resp);
        $(".preloader").fadeOut();
      }
    )
  }
  // End of List Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Factor List // Students [ student_ ]
  SubjectAttainment: any = new Object()
  subject_detail_attainment() {
    this.shopService.subject_attainment_detail({ subject: this.Subject.subject_id }).subscribe(
      resp => {
        this.SubjectAttainment = resp[0]
        console.log(this.SubjectAttainment);
        $(".preloader").fadeOut();
      }
    )
  }

  update_attainment() {
    this.isLoading = true
    this.shopService.subject_attainment_edit(this.SubjectAttainment).subscribe(
      resp => {
        console.log(resp)
        this.isLoading = false
        // this.subject_detail_attainment()
      },
    );
  }

  update_po_map(factor, po) {
    this.isLoading = true
    console.log(factor[po]);

    factor[po] == undefined ? factor[po] = null : factor[po] = factor[po]
    factor[po] == '' ? factor[po] = null : factor[po] = factor[po]

    this.shopService.course_o_edit(factor).subscribe(
      resp => {
        console.log(resp)
        this.isLoading = false
        // this.list_coutcomes()
      },
    );
  }
  // End of List Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Factor List // Factors [ classroom_ ]
  COutcomes: any = [];
  list_coutcomes() {
    this.shopService.course_o_list(this.Subject).subscribe(
      resp => {
        this.COutcomes = resp
        console.log(resp);
        $(".preloader").fadeOut();
      }
    )
  }
  // End of List Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Factor [ classroom_ ]
  create_factor(form: NgForm) {
    this.Attainment.subject = this.Subject.subject_id
    console.log(this.Attainment);

    this.shopService.attainment_create(this.Attainment).subscribe(
      resp => {
        console.log(resp)
        this.list_attainments()
        this.Attainment = new Object()
        $('#create-attainment-modal').modal('hide');
        form.reset()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  // End Create AttainmentField
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  toAttDetail(factor) {
    console.log(factor);
    let navigationExtras: NavigationExtras = {
      state: {
        attainment_id: factor.attainment_id,
        subject_id: this.Subject.subject_id
      }
    }
    Storage.set({
      key: 'attainment_id',
      value: factor.attainment_id
    });
    Storage.set({
      key: 'subject_id',
      value: this.Subject.subject_id
    });
    this.router.navigate(['dashboard/attain-management'], navigationExtras)
  }
  toExamDetail(factor) {
    console.log(factor);
    let navigationExtras: NavigationExtras = {
      state: {
        exam_id: factor.exam_id,
        subject_id: this.Subject.subject_id
      }
    }
    Storage.set({
      key: 'exam_id',
      value: factor.exam_id
    });
    this.router.navigate(['dashboard/exam-management'], navigationExtras)
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

}
