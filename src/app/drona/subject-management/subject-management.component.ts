import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatAccordion } from '@angular/material/expansion';
import { NavigationExtras, Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { ShopService } from 'src/app/backend/shop.service';


declare var $: any;
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;
@Component({
  selector: 'app-subject-management',
  templateUrl: './subject-management.component.html',
  styleUrls: ['./subject-management.component.scss']
})
export class SubjectManagementComponent implements OnInit {

  @ViewChild(MatAccordion) accordion: MatAccordion;

  constructor(private router: Router, private shopService: ShopService) { }


  ngOnInit(): void {

    /////////////////////////////////////////////////////////////////////////////////////////////
    this.list_factor()
    this.list_faculty()
    /////////////////////////////////////////////////////////////////////////////////////////////

  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Variables

  selectedFaculty: any;
  message: any;

  // End Variables
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Factor List // Factors [ subject_ ]
  Factors: any = [];
  Classrooms: any = [];
  list_factor() {
    this.shopService.subject_list().subscribe(
      resp => {
        this.Factors = resp
        console.log(resp);
        this.rerender()
        $(".preloader").fadeOut();
      }
    )
    this.shopService.classroom_list().subscribe(
      resp => {
        this.Classrooms = resp
        console.log(resp);
        $(".preloader").fadeOut();
      }
    )
  }
  // End of List Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Factor [ subject_ ]
  Factor: any = new Object()
  create_factor(form: NgForm) {
    this.Factor.faculty = this.selectedFaculty
    // form.value.calendar_eligible == undefined ? this.Factor.calendar_eligible = false : this.Factor.calendar_eligible = true
    this.shopService.subject_create(this.Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_factor()
        this.Factor = new Object()
        $('#create-factor-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  // End Create Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Category [ subject_ ]
  edit_factor(form: NgForm) {
    console.log(this.Factor);
    this.Factor.faculty = this.selectedFaculty
    this.shopService.subject_edit(this.Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_factor()
        this.Factor = new Object()
        this.selectedFaculty = null
        $('#edit-factor-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Delete Factor [ subject_ ]
  delete_factor(ele: any) {
    this.Factor.subject_id = ele.subject_id
    this.shopService.subject_delete(this.Factor).subscribe(
      resp => {
        this.list_factor()
        $('#edit-factor-modal').modal('hide');

      },
    )
  }
  // End Delete Factor
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Factor List // Factors [ staffs_ ]
  Faculties: any = [];
  list_faculty() {
    this.shopService.staffs_list().subscribe(
      resp => {
        this.Faculties = resp
        console.log(resp);
        this.rerender()
      }
    )

  }
  // End of List Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Misc Functions
  onSelect(e) {
    console.log(e);
    this.selectedFaculty = e.subject_industry_id
    console.log(this.selectedFaculty);
  }

  show_edit(ele: any) {
    console.log(ele);
    this.selectedFaculty = ele.faculty
    this.Factor = ele
    console.log(ele);
    $('#edit-factor-modal').modal('show')
  }

  toSubDetail(factor) {
    console.log(factor);
    let navigationExtras: NavigationExtras = {
      state: {
        subject_id: factor.subject_id
      }
    }
    Storage.set({
      key: 'subject_id',
      value: factor.subject_id
    });
    this.router.navigate(['dashboard/subject-detail'], navigationExtras)
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  isExpand: any = false
  @ViewChild(DataTableDirective, { static: false })
  dtEle: DataTableDirective;
  dtTrigger: Subject<any> = new Subject<any>();
  dtOptions: DataTables.Settings = {};
  // DataTables Functions
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtEle.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }
  togglePanels() {
    if (this.isExpand) { this.accordion.closeAll() }
    else { this.accordion.openAll() }
    this.isExpand = !this.isExpand
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

}
