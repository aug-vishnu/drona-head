import { ShopService } from './../../backend/shop.service';
import { Component, OnInit } from '@angular/core';
import * as XLSX from 'xlsx';

type AOA = any[][];

@Component({
  selector: 'app-student-sheet',
  templateUrl: './student-sheet.component.html',
  styleUrls: ['./student-sheet.component.scss']
})
export class StudentSheetComponent implements OnInit {

  constructor(private shopService: ShopService) { }

  ngOnInit(): void {
    this.list_factor()
  }
  isLoading = false
  data: AOA = [['Registration No.', 'Name', 'Email']];
  wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };
  fileName: string = 'SheetJS.xlsx';

  onFileChange(evt: any) {
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });

      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      this.data = <AOA>(XLSX.utils.sheet_to_json(ws, { header: 1 }));
      console.log(this.data);
    };
    reader.readAsBinaryString(target.files[0]);
  }


  export(): void {
    /* generate worksheet */
    const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(this.data);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, this.fileName);
  }


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Factor [ student_ ]
  Factor: any = new Object()
  create_factor(factor) {
    this.Factor = factor
    this.Factor.password = 'Welcome@123'
    console.log(this.Factor);

    this.shopService.student_create(factor).subscribe(
      resp => {
        console.log(resp)
      },
      error => {
        console.log('error', error);
      }
    );
  }
  // End Create Factor
  /////////////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Factor List // Factors [ subject_ ]
  Factors: any = [];
  Classrooms: any = [];
  list_factor() {
    this.shopService.classroom_list().subscribe(
      resp => {
        this.Classrooms = resp
        console.log(resp);
        $(".preloader").fadeOut();
      }
    )
  }
  // End of List Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  validate_sheet() {
    this.isLoading = true
    this.data.forEach(element => {
      this.Factor.register_number = element[0]
      this.Factor.name = element[1]
      this.Factor.email = element[2]
      this.create_factor(this.Factor)

    });
    this.isLoading = false

  }
  /////////////////////////////////////////////////////////////////////////////////////////////

}
