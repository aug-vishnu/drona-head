import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatAccordion } from '@angular/material/expansion';
import { Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { ShopService } from 'src/app/backend/shop.service';
declare var $: any;

@Component({
  selector: 'app-student-management',
  templateUrl: './student-management.component.html',
  styleUrls: ['./student-management.component.scss']
})
export class StudentManagementComponent implements OnInit {

  @ViewChild(MatAccordion) accordion: MatAccordion;

  constructor(private router: Router, private shopService: ShopService) { }


  ngOnInit(): void {

    /////////////////////////////////////////////////////////////////////////////////////////////
    this.list_factor()
    /////////////////////////////////////////////////////////////////////////////////////////////

  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Variables

  selectedFaculty: any;
  message: any;

  // End Variables
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Factor List // Factors [ student_ ]
  Factors: any = [];
  list_factor() {
    this.shopService.student_list().subscribe(
      resp => {
        this.Factors = resp
        console.log(resp);
        this.rerender()
        this.list_classroom()
        $(".preloader").fadeOut();
      }
    )
  }
  // End of List Factor
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Factor List //  [ Classrooms , _classroom ,  classroom_ ]
  Classrooms: any = [];
  list_classroom() {
    this.shopService.classroom_list().subscribe(
      resp => {
        this.Classrooms = resp
        console.log(resp);
        this.rerender()
      }
    )
  }
  // End of List Factor
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Factor [ student_ ]
  Factor: any = new Object()
  create_factor(form: NgForm) {
    this.Factor.faculty = this.selectedFaculty
    // form.value.calendar_eligible == undefined ? this.Factor.calendar_eligible = false : this.Factor.calendar_eligible = true
    this.shopService.student_create(this.Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_factor()
        this.Factor = new Object()
        this.selectedFaculty = null
        $('#create-factor-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  // End Create Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Category [ student_ ]
  edit_factor(form: NgForm) {
    console.log(this.Factor);
    this.Factor.faculty = this.selectedFaculty
    this.shopService.student_edit(this.Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_factor()
        this.Factor = new Object()
        this.selectedFaculty = null
        $('#edit-factor-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Delete Factor [ student_ ]
  delete_factor(ele: any) {
    this.Factor.student_id = ele.student_id
    this.shopService.student_delete(this.Factor).subscribe(
      resp => {
        this.list_factor()
        $('#edit-factor-modal').modal('hide');

      },
    )
  }
  // End Delete Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Misc Functions
  onSelect(e) {
    console.log(e);
    this.selectedFaculty = e.student_industry_id
    console.log(this.selectedFaculty);
  }

  show_edit(ele: any) {
    console.log(ele);
    this.selectedFaculty = ele.faculty
    this.Factor = ele
    console.log(ele);
    $('#edit-factor-modal').modal('show')
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  isExpand: any = false
  @ViewChild(DataTableDirective, { static: false })
  dtEle: DataTableDirective;
  dtTrigger: Subject<any> = new Subject<any>();
  dtOptions: DataTables.Settings = {};
  // DataTables Functions
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtEle.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }
  togglePanels() {
    if (this.isExpand) { this.accordion.closeAll() }
    else { this.accordion.openAll() }
    this.isExpand = !this.isExpand
  }
  /////////////////////////////////////////////////////////////////////////////////////////////




}
