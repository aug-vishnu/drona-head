import { ExamService } from './../../backend/exam.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ShopService } from 'src/app/backend/shop.service';
import { Component, OnInit } from '@angular/core';
declare var $: any;
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;
@Component({
  selector: 'app-attainment-management',
  templateUrl: './attainment-management.component.html',
  styleUrls: ['./attainment-management.component.scss']
})
export class AttainmentManagementComponent implements OnInit {



  Subject: any = new Object()
  FieldType: any = new Object()
  Attainment: any = new Object()
  isLoading = false

  constructor(private router: Router, private shopService: ShopService, private examService: ExamService) {
    try {
      console.log("Fetched from Nav Extras");
      this.Subject.subject_id = this.router.getCurrentNavigation().extras.state.subject_id
      this.Attainment.attainment_id = this.router.getCurrentNavigation().extras.state.attainment_id
    } catch (error) {
      console.log("!! Fetched from Storage");
      this.getItem()
      this.getAttainment()
    }
  }
  async getAttainment() {
    const { value } = await Storage.get({ key: 'subject_id' });
    this.Subject.subject_id = value
  }

  async getItem() {
    const { value } = await Storage.get({ key: 'attainment_id' });
    this.Attainment.attainment_id = value
    console.log('Got item: ', value);
  }

  ngOnInit(): void {
    console.log(this.Subject);
    console.log(this.Attainment);
    this.list_students()
    this.list_attainments()
  }



  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Factor [ classroom_ ]
  AttainmentField: any = new Object()
  create_factor(form: NgForm) {
    this.AttainmentField.attainment = this.Attainment.attainment_id
    this.AttainmentField.subject = this.Subject.subject_id
    this.AttainmentField.course_o = form.value.course_o

    console.log(this.AttainmentField);

    this.shopService.attainment_field_create(this.AttainmentField).subscribe(
      resp => {
        console.log(resp)
        this.list_attainments()
        this.AttainmentField = new Object()
        $('#create-attainment-modal').modal('hide');
        form.reset()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  // End Create AttainmentField
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Category [ classroom_ ]
  edit_factor(form: NgForm) {
    console.log(this.AttainmentField);
    this.shopService.classroom_edit(this.AttainmentField).subscribe(
      resp => {
        console.log(resp)
        this.list_attainments()
        this.AttainmentField = new Object()
        $('#edit-factor-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////




  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Factor [ classroom_ ]
  create_field_type(form: NgForm) {
    this.shopService.attainment_field_type_create(this.FieldType).subscribe(
      resp => {
        console.log(resp)
        this.list_attainments()
        this.FieldType = new Object()
        $('#create-attainment-modal').modal('hide');
        form.reset()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  // End Create AttainmentField
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Category [ classroom_ ]
  Value: any;
  async update_all_value(factor) {
    this.isLoading = true
    const values = this.Attainment['values']
    for (let index2 = 0; index2 < values.length; index2++) {
      const value = values[index2];
      console.log(value.field_value + ' - ' + value.field_value_id);
      this.shopService.field_value_edit({ 'field_value': value.field_value, 'field_value_id': value.field_value_id, }).subscribe(
        resp => {
          console.log(resp)
        },
      );
    }
    await this.detail_attainment()
  }
  update_value(factor) {
    this.isLoading = true
    if (factor.field_value == '-' && factor.field_value == 'ab' && factor.field_value == 'AB') {
      factor.field_value = 'AB'
    }
    this.shopService.field_value_edit({ 'field_value': factor.field_value, 'field_value_id': factor.field_value_id, }).subscribe(
      resp => {
        console.log(resp)
        this.isLoading = false
      },
    );
  }
  update_threshold(factor) {
    this.isLoading = true
    this.shopService.attainment_field_edit(factor).subscribe(
      resp => {
        console.log(resp)
        this.detail_attainment()
        this.isLoading = false
      },
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Delete AttainmentField [ classroom_ ]
  delete_factor(ele: any) {
    this.AttainmentField.classroom_id = ele.classroom_id
    this.shopService.classroom_delete(this.AttainmentField).subscribe(
      resp => {
        this.list_attainments()
        $('#edit-factor-modal').modal('hide');
      },
    )
  }
  // End Delete AttainmentField
  /////////////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Factor List // Students [ student_ ]
  Attainments: any = [];
  FieldTypes: any = [];
  Exams: any = [];
  list_attainments() {
    this.shopService.attainment_list({ subject: this.Subject.subject_id }).subscribe(
      resp => {
        this.Attainments = resp
        console.log(resp);
        this.list_coutcomes()
      }
    )
    this.shopService.attainment_field_type_list().subscribe(
      resp => {
        this.FieldTypes = resp
        console.log(resp);
      }
    )
    this.examService.exam_list({ subject_id: this.Subject.subject_id }).subscribe(
      resp => {
        this.Exams = resp
        console.log(resp);
      }
    )

  }
  // End of List Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Factor List // Factors [ classroom_ ]
  COutcomes: any = [];
  list_coutcomes() {
    this.shopService.course_o_list(this.Subject).subscribe(
      resp => {
        this.COutcomes = resp
        console.log(resp);
        this.detail_attainment()
      }
    )
  }
  // End of List Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Factor List // Students [ student_ ]
  detail_attainment() {
    this.shopService.attainment_detail({ attainment_id: this.Attainment.attainment_id }).subscribe(
      resp => {
        this.Attainment = resp[0]
        console.log(this.Attainment);
        $(".preloader").fadeOut();
        this.isLoading = false

      }
    )
  }
  // End of List Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Factor List // Students [ student_ ]
  Students: any = [];
  list_students() {
    this.shopService.student_list().subscribe(
      resp => {
        this.Students = resp
        console.log(resp);
      }
    )
  }
  // End of List Factor
  /////////////////////////////////////////////////////////////////////////////////////////////

}
