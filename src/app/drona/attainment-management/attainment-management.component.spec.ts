import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AttainmentManagementComponent } from './attainment-management.component';

describe('AttainmentManagementComponent', () => {
  let component: AttainmentManagementComponent;
  let fixture: ComponentFixture<AttainmentManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AttainmentManagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AttainmentManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
