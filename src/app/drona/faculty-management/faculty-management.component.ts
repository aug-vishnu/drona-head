import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatAccordion } from '@angular/material/expansion';
import { Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { ShopService } from 'src/app/backend/shop.service';
declare var $: any;

@Component({
  selector: 'app-faculty-management',
  templateUrl: './faculty-management.component.html',
  styleUrls: ['./faculty-management.component.scss']
})
export class FacultyManagementComponent implements OnInit {
  @ViewChild(MatAccordion) accordion: MatAccordion;

  constructor(private router: Router, private shopService: ShopService) { }


  ngOnInit(): void {

    /////////////////////////////////////////////////////////////////////////////////////////////
    this.list_factor()
    /////////////////////////////////////////////////////////////////////////////////////////////

  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Variables

  selectedFaculty: any;
  message: any;

  // End Variables
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Factor List // Factors [ faculty_ ]
  Factors: any = [];
  list_factor() {
    this.shopService.faculty_list().subscribe(
      resp => {
        this.Factors = resp
        console.log(resp);
        this.rerender()
        $(".preloader").fadeOut();
      }
    )
  }
  // End of List Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Factor [ faculty_ ]
  Factor: any = new Object()
  create_factor(form: NgForm) {
    this.Factor.faculty = this.selectedFaculty
    // form.value.calendar_eligible == undefined ? this.Factor.calendar_eligible = false : this.Factor.calendar_eligible = true
    this.shopService.faculty_create(this.Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_factor()
        this.Factor = new Object()
        this.selectedFaculty = null
        $('#create-factor-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  // End Create Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Category [ faculty_ ]
  edit_factor(form: NgForm) {
    console.log(this.Factor);
    this.Factor.faculty = this.selectedFaculty
    this.shopService.faculty_edit(this.Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_factor()
        this.Factor = new Object()
        this.selectedFaculty = null
        $('#edit-factor-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Delete Factor [ faculty_ ]
  delete_factor(ele: any) {
    console.log(ele);
    
    this.Factor.faculty_id = ele.faculty_id
    console.log(this.Factor);
    
    this.shopService.faculty_delete(this.Factor).subscribe(
      resp => {
        this.list_factor()
        console.log(resp);
        
        $('#edit-factor-modal').modal('hide');

      },
    )
  }
  // End Delete Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Misc Functions
  onSelect(e) {
    console.log(e);
    this.selectedFaculty = e.faculty_industry_id
    console.log(this.selectedFaculty);
  }

  show_edit(ele: any) {
    console.log(ele);
    this.selectedFaculty = ele.faculty
    this.Factor = ele
    console.log(ele);
    $('#edit-factor-modal').modal('show')
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  isExpand: any = false
  @ViewChild(DataTableDirective, { static: false })
  dtEle: DataTableDirective;
  dtTrigger: Subject<any> = new Subject<any>();
  dtOptions: DataTables.Settings = {};
  // DataTables Functions
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtEle.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }
  togglePanels() {
    if (this.isExpand) { this.accordion.closeAll() }
    else { this.accordion.openAll() }
    this.isExpand = !this.isExpand
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


}
