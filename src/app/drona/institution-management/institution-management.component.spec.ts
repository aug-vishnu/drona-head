import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InstitutionManagementComponent } from './institution-management.component';

describe('InstitutionManagementComponent', () => {
  let component: InstitutionManagementComponent;
  let fixture: ComponentFixture<InstitutionManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InstitutionManagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InstitutionManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
