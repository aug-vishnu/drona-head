import { NgForm } from '@angular/forms';
import { ExamService } from './../../backend/exam.service';
import { ShopService } from './../../backend/shop.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
declare var $: any;
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;
@Component({
  selector: 'app-exam-management',
  templateUrl: './exam-management.component.html',
  styleUrls: ['./exam-management.component.scss']
})
export class ExamManagementComponent implements OnInit {

  /////////////////////////////////////////////////////////////////////////////////////////////
  Subject: any = new Object()
  Exam: any = new Object()
  Responses: any = []
  Students: any = []
  isEdit = false
  isPortal = true
  constructor(private router: Router, private shopService: ShopService, private examService: ExamService) {
    try {
      console.log("Fetched from Nav Extras");
      this.Exam.exam_id = this.router.getCurrentNavigation().extras.state.exam_id
      console.log(this.Exam);
    } catch (error) {
      console.log("!! Fetched from Storage");
      this.getItem()
    }
  }
  //  ///////////////////////////////////////////////////////////////////////////////////////////
  async getItem() {
    const { value } = await Storage.get({ key: 'exam_id' });
    this.Exam.exam_id = value
    console.log('Got item: ', value);
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  ngOnInit(): void {
    this.exam_detail()
  }



  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Exam Detail // Exam [ exam_ ]
  exam_detail() {
    this.examService.exam_response_list({ exam_id: this.Exam.exam_id }).subscribe(
      resp => {
        this.Responses = resp[0]
        console.log(this.Responses);
      }
    )
    this.examService.exam_detail({ exam_id: this.Exam.exam_id }).subscribe(
      resp => {
        this.Exam = resp[0]
        this.Subject.subject_id = this.Exam.subject_id
        console.log(this.Exam);
        this.list_coutcomes()
      }
    )
    this.shopService.student_list().subscribe(
      resp => {
        this.Students = resp
        console.log(resp);
      }
    )
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Factor List // Factors [ classroom_ ]
  COutcomes: any = [];
  list_coutcomes() {
    this.shopService.course_o_list(this.Subject).subscribe(
      resp => {
        this.COutcomes = resp[0]
        console.log(resp);
        $(".preloader").fadeOut();
      }
    )
  }
  // End of List Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Division [ division_ ]
  Division: any = new Object()
  create_factor(form: NgForm) {
    this.Division.exam_id = this.Exam.exam_id
    this.Division.division_type = 1
    console.log(this.Division);

    this.examService.division_create(this.Division).subscribe(
      resp => {
        console.log(resp)
        this.exam_detail()
        this.Division = new Object()
        form.reset()
        $('#create-division-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  // End Create Division
  /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Division [ division_ ]
  edit_factor(form: NgForm) {
    console.log(this.Division);
    this.examService.division_edit(this.Division).subscribe(
      resp => {
        console.log(resp)
        this.exam_detail()
        this.Division = new Object()
        $('#edit-division-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Delete Division [ division_ ]
  delete_factor(ele: any) {
    this.Division.division_id = ele.division_id
    this.examService.division_delete(this.Division).subscribe(
      resp => {
        this.exam_detail()
        $('#edit-factor-modal').modal('hide');
      },
    )
  }
  // End Delete Division
  /////////////////////////////////////////////////////////////////////////////////////////////




  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Question [ question_ ]
  Question: any = new Object()
  create_question(form: NgForm) {
    this.Question.division_id = this.Division.division_id
    this.Question.question_mark = this.Division.mark_per_question
    this.Question.question_type = 2
    this.examService.question_create(this.Question).subscribe(
      resp => {
        console.log(resp)
        this.exam_detail()
        this.Question = new Object()
        $('#question-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  set_division(division) {
    this.Division = division
  }
  // End Create Question
  /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Question [ question_ ]
  pop_edit_question(factor: any) {
    this.Question = factor
    this.isEdit = true
    $('#question-modal').modal('show');
  }
  edit_question(form: NgForm) {
    console.log(this.Question);
    this.examService.question_edit(this.Question).subscribe(
      resp => {
        console.log(resp)
        this.exam_detail()
        this.Question = new Object()
        this.isEdit = false
        $('#question-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Delete Question [ question_ ]
  delete_question(ele: any) {
    this.Question.question_id = ele.question_id
    this.examService.question_delete(this.Question).subscribe(
      resp => {
        this.exam_detail()
        $('#edit-factor-modal').modal('hide');
      },
    )
  }
  // End Delete Question
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start of Misc
  show_question_or_portal() {
    this.isPortal = !this.isPortal
  }
  update_value(factor) {
    // this.isLoading = true
    if (factor.question_mark == '-' && factor.question_mark == '' && factor.question_mark == 'AB') {
      factor.question_mark = ''
    }
    this.examService.exam_response_edit({ 'question_mark': factor.question_mark, 'response_id': factor.response_id, }).subscribe(
      resp => {
        console.log(resp)
        // this.isLoading = false
      },
    );
  }
  // End of Misc
  /////////////////////////////////////////////////////////////////////////////////////////////
}
