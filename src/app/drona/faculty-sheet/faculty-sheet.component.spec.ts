import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FacultySheetComponent } from './faculty-sheet.component';

describe('FacultySheetComponent', () => {
  let component: FacultySheetComponent;
  let fixture: ComponentFixture<FacultySheetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FacultySheetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FacultySheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
