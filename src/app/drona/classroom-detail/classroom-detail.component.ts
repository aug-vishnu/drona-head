import { ShopService } from './../../backend/shop.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
declare var $: any;
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Label } from 'ng2-charts';
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;

@Component({
  selector: 'app-classroom-detail',
  templateUrl: './classroom-detail.component.html',
  styleUrls: ['./classroom-detail.component.scss']
})
export class ClassroomDetailComponent implements OnInit {

  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public barChartLabels: Label[] = ['PO1', 'PO2', 'PO3', 'PO4', 'PO5', 'PO6', 'PO7', 'PO8', 'PO9', 'PO10', 'PO11', 'PO12', 'PSO1', 'PSO2', 'PSO3'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  // public barChartPlugins = [pluginDataLabels];

  Classroom: any = new Object()
  public barChartData: ChartDataSets[] = [
    {
      data: [
        // this.Classroom.attainment_po1,
        // this.Classroom.attainment_po2,
        // this.Classroom.attainment_po3,
        // this.Classroom.attainment_po4,
        // this.Classroom.attainment_po5,
        // this.Classroom.attainment_po6,
        // this.Classroom.attainment_po7,
        // this.Classroom.attainment_po8,
        // this.Classroom.attainment_po9,
        // this.Classroom.attainment_po10,
        // this.Classroom.attainment_po11,
        // this.Classroom.attainment_po12,

        // this.Classroom.attainment_pso1,
        // this.Classroom.attainment_pso2,
        // this.Classroom.attainment_pso3,
      ], label: 'PO Attainments'
    },
  ];

  constructor(private router: Router, private shopService: ShopService) {
    try {
      console.log("Fetched from Nav Extras");
      this.Classroom.classroom_id = this.router.getCurrentNavigation().extras.state.classroom_id
      console.log(this.Classroom);
    } catch (error) {
      console.log("!! Fetched from Storage");
      this.getItem()
    }
  }
  ngOnInit(): void {
    this.classroom_detail()
  }

  async getItem() {
    const { value } = await Storage.get({ key: 'classroom_id' });
    this.Classroom.classroom_id = value
    console.log('Got item: ', value);
  }

  isLoading = false

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Factor List // Students [ student_ ]
  classroom_detail() {
    this.shopService.classroom_detail({ classroom_id: this.Classroom.classroom_id }).subscribe(
      resp => {
        this.Classroom = resp[0]
        console.log(resp[0]);

        console.log(this.Classroom);
        $(".preloader").fadeOut();
        this.barChartData[0].data = [
          this.Classroom.attainment_po1,
          this.Classroom.attainment_po2,
          this.Classroom.attainment_po3,
          this.Classroom.attainment_po4,
          this.Classroom.attainment_po5,
          this.Classroom.attainment_po6,
          this.Classroom.attainment_po7,
          this.Classroom.attainment_po8,
          this.Classroom.attainment_po9,
          this.Classroom.attainment_po10,
          this.Classroom.attainment_po11,
          this.Classroom.attainment_po12,

          this.Classroom.attainment_pso1,
          this.Classroom.attainment_pso2,
          this.Classroom.attainment_pso3,
        ]
      }
    )
    console.log(this.barChartData[0].data);
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public randomize(): void {
    // Only Change 3 values
    this.barChartData[0].data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
  }


}
