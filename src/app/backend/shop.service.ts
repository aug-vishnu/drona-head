import { Observable } from 'rxjs';
import { environment } from './../../environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ShopService {

  constructor(private http: HttpClient) { }
  private APIurl = environment.APIurl + '/'
  // private shop_id = environment.shop_id
  // private shop = { shop_id: this.shop_id }

  ///////////////////////////////////////////////////////////////////////////////////////
  // Institution CRUD
  institution_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'core/institution-cud/', params)
  }

  institution_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'core/institution-cud/', params)
  }

  institution_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        institution_id: params.institution_id
      },
    };
    return this.http.delete(this.APIurl + 'core/institution-cud/', options)
  }

  institution_detail(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'core/institution-detail/', params)
  }

  institution_list(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'core/institution-list/', params)
  }

  ////////////////////////////////////////////////////////////////////////////////////////
  // Department CRUD
  department_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'core/department-cud/', params)
  }

  department_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'core/department-cud/', params)
  }

  department_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        department_id: params.department_id
      },
    };
    return this.http.delete(this.APIurl + 'core/department-cud/', options)
  }

  department_detail(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'core/department-detail/', params)
  }

  department_list(): Observable<any> {
    return this.http.post(this.APIurl + 'core/department-list/', {})
  }
  staffs_list(): Observable<any> {
    return this.http.post(this.APIurl + 'auth/faculty-list/', {})
  }

  ////////////////////////////////////////////////////////////////////////////////////////
  // ClassRoom CRUD
  classroom_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'core/classroom-cud/', params)
  }

  classroom_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'core/classroom-cud/', params)
  }

  classroom_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        classroom_id: params.classroom_id
      },
    };
    return this.http.delete(this.APIurl + 'core/classroom-cud/', options)
  }

  classroom_detail(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'core/classroom-detail/', params)
  }

  classroom_list(): Observable<any> {
    return this.http.post(this.APIurl + 'core/classroom-list/', {})
  }
  ////////////////////////////////////////////////////////////////////////////////////////
  // Subject CRUD
  subject_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'core/subject-cud/', params)
  }

  subject_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'core/subject-cud/', params)
  }

  subject_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        subject_id: params.subject_id
      },
    };
    return this.http.delete(this.APIurl + 'core/subject-cud/', options)
  }

  subject_detail(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'core/subject-detail/', params)
  }

  subject_list(): Observable<any> {
    return this.http.post(this.APIurl + 'core/subject-list/', {})
  }
  ////////////////////////////////////////////////////////////////////////////////////////


  ////////////////////////////////////////////////////////////////////////////////////////
  // Faculty CRUD
  faculty_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'auth/faculty-cud/', params)
  }

  faculty_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'auth/faculty-cud/', params)
  }

  faculty_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        faculty_id: params.faculty_id
      },
    };

    return this.http.delete(this.APIurl + 'auth/faculty-cud/', options)
  }

  faculty_detail(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'auth/faculty-detail/', params)
  }

  faculty_list(): Observable<any> {
    return this.http.post(this.APIurl + 'auth/faculty-list/', {})
  }
  ////////////////////////////////////////////////////////////////////////////////////////


  ////////////////////////////////////////////////////////////////////////////////////////
  // Student CRUD
  student_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'auth/student-cud/', params)
  }

  student_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'auth/student-cud/', params)
  }

  student_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        student_id: params.student_id
      },
    };
    return this.http.delete(this.APIurl + 'auth/student-cud/', options)
  }

  student_detail(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'auth/student-detail/', params)
  }

  student_list(): Observable<any> {
    return this.http.post(this.APIurl + 'auth/student-list/', {})
  }
  ////////////////////////////////////////////////////////////////////////////////////////


  ////////////////////////////////////////////////////////////////////////////////////////
  // Course Outcome CRUD
  course_o_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'exam/course-o-cud/', params)
  }

  course_o_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'exam/course-o-cud/', params)
  }

  course_o_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        course_o_id: params.course_o_id
      },
    };
    return this.http.delete(this.APIurl + 'exam/course-o-cud/', options)
  }

  course_o_detail(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'exam/course-o-detail/', params)
  }

  course_o_list(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'exam/course-o-list/', params)
  }
  ////////////////////////////////////////////////////////////////////////////////////////


  ////////////////////////////////////////////////////////////////////////////////////////
  // Course Outcome CRUD [attainment , attainment/ , attainment ]
  attainment_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'attainment/attainment-cud/', params)
  }

  attainment_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'attainment/attainment-cud/', params)
  }

  attainment_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        attainment_id: params.attainment_id
      },
    };
    return this.http.delete(this.APIurl + 'attainment/attainment-cud/', options)
  }

  attainment_detail(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'attainment/attainment-detail/', params)
  }

  subject_attainment_detail(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'attainment/subject-attainment-detail/', params)
  }

  subject_attainment_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'attainment/subject-attainment-cud/', params)
  }

  attainment_list(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'attainment/attainment-list/', params)
  }
  ////////////////////////////////////////////////////////////////////////////////////////


  ////////////////////////////////////////////////////////////////////////////////////////
  // Course Outcome CRUD [attainment_field_type_ , attainment/ , attainment-field-type- ]
  attainment_field_type_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'attainment/attainment-field-type-cud/', params)
  }

  attainment_field_type_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'attainment/attainment-field-type-cud/', params)
  }

  attainment_field_type_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        attainment_field_type_id: params.attainment_field_type_id
      },
    };
    return this.http.delete(this.APIurl + 'attainment/attainment-field-type-cud/', options)
  }

  attainment_field_type_list(): Observable<any> {
    return this.http.post(this.APIurl + 'attainment/attainment-field-type-list/', {})
  }
  ///////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Course Outcome CRUD [attainment_field_ , attainment/ , attainment ]
  attainment_field_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'attainment/attainment-field-cud/', params)
  }

  attainment_field_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'attainment/attainment-field-cud/', params)
  }

  attainment_field_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        attainment_field_id: params.attainment_field_id
      },
    };
    return this.http.delete(this.APIurl + 'attainment/attainment-field-cud/', options)
  }
  ////////////////////////////////////////////////////////////////////////////////////////


  ////////////////////////////////////////////////////////////////////////////////////////
  // Course Outcome CRUD [field_value_ , attainment/attainment-value ]
  field_value_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'attainment/field-value-cud/', params)
  }

  field_value_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'attainment/field-value-cud/', params)
  }

  field_value_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        field_value_id: params.field_value_id
      },
    };
    return this.http.delete(this.APIurl + 'attainment/field-value-cud/', options)
  }
  ////////////////////////////////////////////////////////////////////////////////////////
}
