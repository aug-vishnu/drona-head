import { Observable } from 'rxjs';
import { environment } from './../../environments/environment.prod';
import { HttpBackend, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


import { Plugins } from '@capacitor/core'
const { Storage } = Plugins


@Injectable({
  providedIn: 'root'
})
export class CommonService {



  // This http client will disable the interceptor and send non authenticated signals

  constructor(private http: HttpClient, handler: HttpBackend) {
    this.http = new HttpClient(handler);
  }

  private readonly APIurl = environment.APIurl + '/'
  private shop_id = 'environment.shop_id'
  private shop = { shop_id: this.shop_id }


  // Basic Methods
  get_token() {
    try {
      return localStorage.getItem('access')
    } catch (error) {
      return Storage.get({ key: 'access' })
    }
  }
  ///////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////////////////
  // Essentials

  login_with_creds(credentials: any): Observable<any> {
    return this.http.post<any>(this.APIurl + "auth/token/", credentials)
  }

  loggedIn() {
    try {
      return !!localStorage.getItem('access')
    } catch (error) {
      return !!Storage.get({ key: 'access' })
    }
  }

  check_names(data: any) {
    return this.http.post<any>(this.APIurl + "allaccount/", data)
  }

  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////



  ///////////////////////////////////////////////////////////////////////////////////////
  // Basic Lists Dump
  shop_detail(): Observable<any> {
    return this.http.post(this.APIurl + 'auth/shop-detail/', { ...this.shop })
  }
  ///////////////////////////////////////////////////////////////////////////////////////


}
