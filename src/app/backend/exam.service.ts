import { environment } from './../../environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ExamService {


  constructor(private http: HttpClient) { }
  private APIurl = environment.APIurl + '/'
  // private shop_id = environment.shop_id
  // private shop = { shop_id: this.shop_id }

  ///////////////////////////////////////////////////////////////////////////////////////
  // Institution CRUD
  exam_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'exam/exam-cud/', params)
  }

  exam_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'exam/exam-cud/', params)
  }

  exam_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        exam_id: params.exam_id
      },
    };
    return this.http.delete(this.APIurl + 'exam/exam-cud/', params)
  }

  exam_detail(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'exam/exam-detail/', params)
  }

  exam_list(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'exam/exam-list/', params)
  }

  ////////////////////////////////////////////////////////////////////////////////////////

  ///////////////////////////////////////////////////////////////////////////////////////
  // Exam Response CRUD
  exam_response_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'exam/response-cud/', params)
  }

  exam_response_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'exam/response-cud/', params)
  }

  exam_response_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        exam_response_id: params.exam_response_id
      },
    };
    return this.http.delete(this.APIurl + 'exam/response-cud/', params)
  }

  exam_response_detail(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'exam/response-detail/', params)
  }

  exam_response_list(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'exam/response-list/', params)
  }

  ////////////////////////////////////////////////////////////////////////////////////////
  // Division CRUD
  division_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'exam/division-cud/', params)
  }

  division_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'exam/division-cud/', params)
  }

  division_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        division_id: params.division_id
      },
    };
    return this.http.delete(this.APIurl + 'exam/division-cud/', params)
  }

  division_detail(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'exam/division-detail/', params)
  }

  division_list(): Observable<any> {
    return this.http.post(this.APIurl + 'exam/division-list/', {})
  }

  ////////////////////////////////////////////////////////////////////////////////////////
  // Question CRUD
  question_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'exam/question-cud/', params)
  }

  question_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'exam/question-cud/', params)
  }

  question_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        question_id: params.question_id
      },
    };
    return this.http.delete(this.APIurl + 'exam/question-cud/', params)
  }

  question_detail(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'exam/question-detail/', params)
  }

  question_list(): Observable<any> {
    return this.http.post(this.APIurl + 'exam/question-list/', {})
  }
  ////////////////////////////////////////////////////////////////////////////////////////


}
