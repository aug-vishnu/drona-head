import { CommonService } from './common.service';
import { Injectable, Injector } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService {

  /*////////////////////////////////////////////////////////////////////////////////////////////

   For the interception add ng g s interceptor and add this injects this token
   to avoid injection refer Shopservice httpclient module there

   For Guard :
   ng g guard backend/auth


  */////////////////////////////////////////////////////////////////////////////////////////////

  constructor(private injector: Injector) { }

  intercept(req, next) {
    let commonService = this.injector.get(CommonService);
    let token_req = req.clone({
      setHeaders: {
        Authorization: `Bearer ${commonService.get_token()}`
      }
    })
    // console.log(token_req);

    return next.handle(token_req)
  }

}
