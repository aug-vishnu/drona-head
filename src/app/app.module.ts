import { ExamService } from './backend/exam.service';
import { ShopService } from './backend/shop.service';
import { AuthGuard } from './backend/auth.guard';
import { CommonService } from './backend/common.service';
import { InterceptorService } from './backend/interceptor.service';
import { LoginComponent } from './auth/login/login.component';
import { DashboardHomeComponent } from './dashboard/dashboard-home/dashboard-home.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardNavComponent } from './dashboard/dashboard-nav/dashboard-nav.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DataTablesModule } from 'angular-datatables';
import { ToastrModule } from 'ngx-toastr';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

// For Google OAuth
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';


// Material Imports
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { MatFormFieldModule } from '@angular/material/form-field';
import { InstitutionManagementComponent } from './drona/institution-management/institution-management.component';
import { DepartmentManagementComponent } from './drona/department-management/department-management.component';
import { FacultyManagementComponent } from './drona/faculty-management/faculty-management.component';
import { ClassroomManagementComponent } from './drona/classroom-management/classroom-management.component';
import { SubjectManagementComponent } from './drona/subject-management/subject-management.component';
import { StudentManagementComponent } from './drona/student-management/student-management.component';
import { ExamManagementComponent } from './drona/exam-management/exam-management.component';
import { SubjectDetailComponent } from './drona/subject-detail/subject-detail.component';
import { ClassroomDetailComponent } from './drona/classroom-detail/classroom-detail.component';
import { AttainmentManagementComponent } from './drona/attainment-management/attainment-management.component';
import { StudentSheetComponent } from './drona/student-sheet/student-sheet.component';
import { FacultySheetComponent } from './drona/faculty-sheet/faculty-sheet.component';
import { RightBarComponent } from './dashboard/right-bar/right-bar.component';
import { SideBarComponent } from './dashboard/side-bar/side-bar.component';
import { TopBarComponent } from './dashboard/top-bar/top-bar.component';
import { DashboardFooterComponent } from './dashboard/dashboard-footer/dashboard-footer.component';



@NgModule({
  declarations: [
    AppComponent,
    DashboardHomeComponent,
    DashboardNavComponent,
    LoginComponent,
    InstitutionManagementComponent,
    DepartmentManagementComponent,
    FacultyManagementComponent,
    ClassroomManagementComponent,
    SubjectManagementComponent,
    StudentManagementComponent,
    ExamManagementComponent,
    SubjectDetailComponent,
    ClassroomDetailComponent,
    AttainmentManagementComponent,
    StudentSheetComponent,
    FacultySheetComponent,
    RightBarComponent,
    SideBarComponent,
    TopBarComponent,
    DashboardFooterComponent,
  ],
  imports: [

    // Basic imports for all ithalam kandipa irukanum
    // npm install -D @angular/cdk ngx-toastr bootstrap angularx-social-login jquery popper.js chart.js font-awesome hammerjs animate.css datatables.net angular-bootstrap-md datatables.net-dt angular-datatables @types/jquery @types/datatables.net --save
    BrowserModule,
    ChartsModule,
    CommonModule,
    BrowserAnimationsModule, // Animations Uhhh
    ToastrModule.forRoot(), // for pop up notification
    HttpClientModule, // to fetch with backend
    FormsModule, // Normal Form
    // ReactiveFormsModule, // for image forms
    DataTablesModule, // All tables
    AppRoutingModule, // app.routing
    // npm i @ng-select / ng - select
    NgSelectModule,


    // Material Components
    MatDatepickerModule,
    MatNativeDateModule,
    MatExpansionModule,
    MatCardModule,
    MatButtonModule,
    MatFormFieldModule,
    MatTabsModule,

  ],
  providers: [
    ShopService,
    ExamService,
    CommonService,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true,
    },
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
