import { ShopService } from './../../backend/shop.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent implements OnInit {

  constructor(private shopService: ShopService) { }
  ngOnInit(): void {
    // this.detail_shop()
    this.Shop;
    console.log("top", this.Shop);

  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Shop Detail // Shop
  Shop: any;
  // detail_shop() {
  //   this.shopService.shop_detail().subscribe(
  //     resp => {
  //     })
  // }
  /////////////////////////////////////////////////////////////////////////////////////////////

  logOut() {
    localStorage.removeItem('token')
  }
}
