import { ShopService } from './../../backend/shop.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { SiteService } from './../../backend/site.service';
import { Subject } from 'rxjs';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
declare var $: any;

@Component({
  selector: 'app-customer-management',
  templateUrl: './customer-management.component.html',
  styleUrls: ['./customer-management.component.scss']
})
export class CustomerManagementComponent implements OnInit {

  constructor(private shopService: ShopService, private router: Router) { }

  ngOnInit(): void {

    /////////////////////////////////////////////////////////////////////////////////////////////
    this.list_factor()
    this.list_industries()
    /////////////////////////////////////////////////////////////////////////////////////////////

  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Variables
  CUSTOMER_TYPE: any = [{ id: 1, value: 'Good' }, { id: 2, value: 'Medium' }, { id: 3, value: 'Bad' }]
  CUSTOMER_FROM: any = [{ id: 1, value: 'Raw Marketing' }, { id: 2, value: 'Reference' }, { id: 3, value: 'Regular Customer' }]
  selectedType: any;
  selectedFrom: any;
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Product List // Products [ customer ]
  Factors: any = [];
  list_factor() {
    this.shopService.customer_list().subscribe(
      resp => {
        this.Factors = resp
        console.log(resp);
        this.rerender()
      }
    )
  }

  Industries: any = [];
  selectedIndustry: any = null;
  list_industries() {
    this.shopService.customer_industry_list().subscribe(
      resp => {
        this.Industries = resp
        console.log(resp);
        $(".preloader").fadeOut();
      }
    )
  }
  // End of List Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Category [ customer ]
  Factor: any = new Object()
  create_factor(form: NgForm) {
    this.Factor.shop_industry = this.selectedIndustry
    this.Factor.customer_type = this.selectedType
    // this.Factor.customer_from = this.selectedFrom
    this.Factor.customer_from = 1
    this.Factor.customer_feedback = null
    form.value.calendar_eligible == undefined ? this.Factor.calendar_eligible = false : this.Factor.calendar_eligible = true

    console.log(this.Factor);
    this.shopService.customer_create(this.Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_factor()
        this.Factor = new Object()
        this.selectedIndustry = null
        this.selectedType = null
        $('#create-factor-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  // End Create Category
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Status [ task ]
  Industry: any = new Object()
  add_industry(form: NgForm) {

    this.shopService.customer_industry_create(form.value).subscribe(
      resp => {
        console.log(resp)
        this.list_industries()
        $('#create-industry-modal').modal('hide');
        this.Industry = new Object()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  // End Create Status
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Category [ customer ]
  Factor: any = new Object()

  edit_factor(form: NgForm) {

    console.log(this.Factor);
    this.Factor.shop_industry = this.selectedIndustry
    this.Factor.customer_type = form.value.customer_type
    this.Factor.customer_from = 1
    // this.Factor.customer_from = form.value.customer_from
    form.value.calendar_eligible == undefined ? this.Factor.calendar_eligible = false : this.Factor.calendar_eligible = true

    this.shopService.customer_edit(this.Factor).subscribe(
      resp => {
        console.log("this.Factor");
        console.log(resp)
        this.list_factor()
        this.Factor = new Object()
        $('#edit-factor-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Delete Factor [ customer -  ]
  delete_factor(ele: any) {
    this.Factor.customer_id = ele.customer_id
    this.shopService.customer_delete(this.Factor).subscribe(
      resp => {
        this.list_factor()
        $('#edit-factor-modal').modal('hide');

      },
    )
  }
  // End Delete Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Delete Factor [ customer -  ]
  delete_industry(ele: any) {
    this.Industry.customer_industry_id = ele.customer_industry_id
    this.shopService.customer_industry_delete(this.Industry).subscribe(
      resp => {
        this.list_factor()
        this.list_industries()
        $('#industry-create-modal').modal('hide');

      },
    )
  }
  // End Delete Factor
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Misc Functions
  onSelect(e) {
    console.log(e);
    this.selectedIndustry = e.customer_industry_id
    console.log(this.selectedIndustry);
  }
  // Misc Functions
  onSelect2(e) {
    console.log(e);
    this.selectedType = e.id
    console.log(this.selectedType);
  }
  onSelect3(e) {
    console.log(e);
    this.selectedFrom = e.id
    console.log(this.selectedFrom);
  }

  show_edit(ele: any) {
    console.log(ele);
    // this.selectedFrom = ele.customer_from
    this.selectedType = ele.customer_type
    this.selectedIndustry = ele.customer_industry_id
    this.Factor = ele
    console.log(ele);
    $('#edit-factor-modal').modal('show')
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  @ViewChild(DataTableDirective, { static: false })
  dtEle: DataTableDirective;
  dtTrigger: Subject<any> = new Subject<any>();
  dtOptions: DataTables.Settings = {};
  // DataTables Functions
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtEle.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

}
