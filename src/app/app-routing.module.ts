import { StudentSheetComponent } from './drona/student-sheet/student-sheet.component';
import { AttainmentManagementComponent } from './drona/attainment-management/attainment-management.component';
import { ClassroomDetailComponent } from './drona/classroom-detail/classroom-detail.component';
import { SubjectDetailComponent } from './drona/subject-detail/subject-detail.component';
import { DepartmentManagementComponent } from './drona/department-management/department-management.component';
import { FacultyManagementComponent } from './drona/faculty-management/faculty-management.component';
import { DashboardHomeComponent } from './dashboard/dashboard-home/dashboard-home.component';
import { AuthGuard } from './backend/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { DashboardNavComponent } from './dashboard/dashboard-nav/dashboard-nav.component';
import { InstitutionManagementComponent } from './drona/institution-management/institution-management.component';
import { ClassroomManagementComponent } from './drona/classroom-management/classroom-management.component';
import { SubjectManagementComponent } from './drona/subject-management/subject-management.component';
import { StudentManagementComponent } from './drona/student-management/student-management.component';
import { ExamManagementComponent } from './drona/exam-management/exam-management.component';




const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full', },

  // Auth
  { path: 'signin', component: LoginComponent },
  // { path:'signup', component: RegisterComponent },


  // Admin-Panel
  {
    path: 'dashboard',
    component: DashboardNavComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', component: DashboardHomeComponent },
      // { path: '', component: ArambHomeComponent },
      { path: 'institution-management', component: InstitutionManagementComponent },

      { path: 'faculty-management', component: FacultyManagementComponent },
      { path: 'faculty-sheet', component: FacultyManagementComponent },
      { path: 'student-management', component: StudentManagementComponent },
      { path: 'student-sheet', component: StudentSheetComponent },

      { path: 'department-management', component: DepartmentManagementComponent },

      { path: 'classroom-management', component: ClassroomManagementComponent },
      { path: 'classroom-detail', component: ClassroomDetailComponent },

      { path: 'subject-management', component: SubjectManagementComponent },
      { path: 'subject-detail', component: SubjectDetailComponent },

      { path: 'exam-management', component: ExamManagementComponent },
      { path: 'attain-management', component: AttainmentManagementComponent },
    ]
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
